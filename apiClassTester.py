import utils.ApiClass as ac

# Example usage:
apiObj = ac.ApiClass()

#login
response = apiObj.login("fian.enfi@gmail.com", "Pisang123")
print(response)
print("code:",response["code"])

#AiTools
#response = apiObj.aitools("/Users/endangfiansyah/Documents/SWC/newxray/919 .jpg", "fian.enfi@gmail.com", response["data"]["token"], "1")
#response = apiObj.aitools("/Users/endangfiansyah/Documents/SWC/27-11-2022/Hoda-279.jpg", "fian.enfi@gmail.com", response["data"]["token"], "1")
print(response)

#update report
data = {
  "email": "fian.enfi@gmail.com",
  "token": response["data"]["token"],
  "box_data": [
    {
      "canvas_id": 2,
      "box_type": 1,
      "box_num": 0,
      "coordinate": [
        1058.2,
        0,
        1309,
        593
      ],
      "confidence": 0,
      "distance": 0,
      "overlap": 10%
    },
    {
      "canvas_id": 3,
      "box_type": 2,
      "box_num": 0,
      "coordinate": [
        1067,
        0,
        1653,
        692
      ],
      "confidence": 0,
      "distance": 0,
      "overlap": 0
    },
    {
      "canvas_id": 4,
      "box_type": 2,
      "box_num": 1,
      "coordinate": [
        1082,
        0,
        1330,
        649
      ],
      "confidence": 0,
      "distance": 0,
      "overlap": 0
    },
    {
      "canvas_id": 5,
      "box_type": 3,
      "box_num": 0,
      "coordinate": [
        30,
        0,
        331,
        644
      ],
      "confidence": 0,
      "distance": 0,
      "overlap": 0
    },
    {
      "canvas_id": 6,
      "box_type": 4,
      "box_num": 0,
      "coordinate": [
        971,
        0,
        1236,
        459
      ],
      "confidence": 0,
      "distance": 0,
      "overlap": 0
    },
    {
      "canvas_id": 7,
      "box_type": 4,
      "box_num": 1,
      "coordinate": [
        991,
        0,
        1342,
        459
      ],
      "confidence": 0,
      "distance": 0,
      "overlap": 0
    },
    {
      "canvas_id": 8,
      "box_type": 4,
      "box_num": 2,
      "coordinate": [
        939,
        0,
        1388,
        555
      ],
      "confidence": 0,
      "distance": 0,
      "overlap": 0
    },
    {
      "canvas_id": 9,
      "box_type": 4,
      "box_num": 3,
      "coordinate": [
        1016,
        0,
        1438,
        772
      ],
      "confidence": 0,
      "distance": 0,
      "overlap": 0
    }
  ]
}
response = apiObj.update_report(data)
print(response)