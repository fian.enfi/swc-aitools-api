def get_iou1(bb1, bb2):
  
    # determine the coordinates of the intersection rectangle
    x_left = max(bb1[0], bb2[0])
    y_top = max(bb1[2], bb2[2])
    x_right = min(bb1[1], bb2[1])
    y_bottom = min(bb1[3], bb2[3])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1[1] - bb1[0]) * (bb1[3] - bb1[2])
    bb2_area = (bb2[1] - bb2[0]) * (bb2[3] - bb2[2])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb2_area)
    assert iou >= 0.0
    assert iou <= 1.0
    return iou

def get_iou(bbox1, bbox2):
  ### List of bbox should be [x1, y1, x2, y2, ...]
  # Calculate area 1
  width_box1 = abs(bbox1[2] - bbox1[0])
  height_box1 = abs(bbox1[3] - bbox1[1])
  area_box1 = width_box1 * height_box1

  # Calculate area 2
  width_box2 = abs(bbox2[2] - bbox2[0])
  height_box2 = abs(bbox2[3] - bbox2[1])
  area_box2 = width_box2 * height_box2

  # Calculate IOU
  x_inter1, y_inter1, x_inter2, y_inter2 = overlap_coord(bbox1, bbox2)
  width_inter = x_inter2 - x_inter1 + 1 if x_inter2 - x_inter1 > 0 else 0
  height_inter = y_inter2 - y_inter1 + 1 if y_inter2 - y_inter1 > 0 else 0
  area_inter = width_inter * height_inter
  area_union = area_box1 + area_box2 - area_inter
  iou = area_inter / area_union
  return iou

def overlap_coord(bbox1, bbox2):
  ### List of bbox should be [x1, y1, x2, y2, ...]
  x_inter1 = max(bbox1[0], bbox2[0])
  y_inter1 = max(bbox1[1], bbox2[1])
  x_inter2 = min(bbox1[2], bbox2[2])
  y_inter2 = min(bbox1[3], bbox2[3])
  return x_inter1, y_inter1, x_inter2, y_inter2