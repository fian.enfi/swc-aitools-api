import cv2 
import numpy as np
import calc_iou as caio
import PIL.Image
import argparse
import os
import sys
import shutil
from scipy.spatial import distance as dist
from pathlib import Path
from os import rmdir

class calculation:
    def msc_msplus(fin,heightmm,widthmm,infile,result,txtfile):
        msplus=[]
        ms=[]
        ms1=[]
        for line1 in fin:
            first_char = line1[0]
            if first_char == "7":
                msplus.append(np.array(line1.split(), dtype=float)) 
            elif first_char == "0":
                ms.append(np.array(line1.split(), dtype=float)) 
            elif first_char == "1":
                ms1.append(np.array(line1.split(), dtype=float))
            else:
                pass    
        for line2 in range (0,len(ms)):
            a=ms[line2][0]
            b=(ms[line2][1])
            b0=b*widthmm
            c=ms[line2][2]
            for line3 in range(0,len(msplus)):
                a1=msplus[line3][0]
                b01=(msplus[line3][1])
                b1=(msplus[line3][1])*widthmm
                c1=msplus[line3][2]
                b02=(b01-(msplus[line3][3]/2))*widthmm
                b03=(b01+(msplus[line3][3]/2))*widthmm
                b2=(b-(ms[line2][3]/2))*widthmm
                b3=(b+(ms[line2][3]/2))*widthmm
                if c<c1:
                    #print('c>c1')
                    cx=(c+((ms[line2][4])/2))
                    cx1=(c-(ms[line2][4]/2))
                    cl=(c1-((msplus[line3][4])/2))
                    cl1=(c1+(msplus[line3][4]/2))
                    bb1=[(b2/widthmm),(b3/widthmm),cx1,cx]
                    bb2=[(b02/widthmm),(b03/widthmm),cl,cl1]
                    if (bb1[0]<bb1[1]) & (bb1[2]<bb1[3]) & (bb2[0]<bb2[1]) & (bb2[2]<bb2[3]):
                        bb1=bb1
                        bb2=bb2
                    else:
                        bb1=[(b2/widthmm),(b3/widthmm),cx,cx1]
                        bb2=[(b02/widthmm),(b03/widthmm),cl1,cl]
                elif c>c1:
                    #print('c>c1')
                    cx=(c-(ms[line2][4]/2))
                    cx1=(c+(ms[line2][4]/2))
                    cl=(c1+(msplus[line3][4]/2))
                    cl1=(c1-(msplus[line3][4]/2))
                    bb1=[(b2/widthmm),(b3/widthmm),cx1,cx]
                    bb2=[(b02/widthmm),(b03/widthmm),cl,cl1]
                    if (bb1[0]<bb1[1]) & (bb1[2]<bb1[3]) & (bb2[0]<bb2[1]) & (bb2[2]<bb2[3]):
                        bb1=bb1
                        bb2=bb2
                    else:
                        bb1=[(b2/widthmm),(b3/widthmm),cx,cx1]
                        bb2=[(b02/widthmm),(b03/widthmm),cl1,cl]
                else:
                    pass
                iou=(caio.get_iou(bb1,bb2))*100
                if iou!=0:
                    iou_format = "{:.2f}".format(iou)
                    num2=str(line2+1)
                    num3=str(line3+1)
                    iou=str(float(iou_format))
                    f = open(result+'/'+txtfile, "a")
                    f.write('overlap msc+ '+num2+' '+iou+'%'+"\n")
                    f.close()
                else:
                    pass

        for line2 in range (0,len(ms1)):
            a=ms1[line2][0]
            b=(ms1[line2][1])
            b0=b*widthmm
            c=ms1[line2][2]
            ca=c*heightmm
            tes=[]
            for line3 in range(0,len(msplus)):
                a1=msplus[line3][0]
                b01=(msplus[line3][1])
                b1=(msplus[line3][1])*widthmm
                c1=msplus[line3][2]
                c1a=c1*heightmm
                b02=(b01-(msplus[line3][3]/2))*widthmm
                b03=(b01+(msplus[line3][3]/2))*widthmm
                b2=(b-(ms1[line2][3]/2))*widthmm
                b3=(b+(ms1[line2][3]/2))*widthmm
                if c<c1:
                    cx=(c+((ms1[line2][4])/2))
                    cx1=(c-(ms1[line2][4]/2))
                    cl=(c1-((msplus[line3][4])/2))
                    cl1=(c1+(msplus[line3][4]/2))
                    bb1=[(b2/widthmm),(b3/widthmm),cx,cx1]
                    bb2=[(b02/widthmm),(b03/widthmm),cl1,cl]
                elif c>c1:
                    cx=(c-(ms1[line2][4]/2))
                    cx1=(c+(ms1[line2][4]/2))
                    cl=(c1+(msplus[line3][4]/2))
                    cl1=(c1-(msplus[line3][4]/2))
                    bb1=[(b2/widthmm),(b3/widthmm),cx,cx1]
                    bb2=[(b02/widthmm),(b03/widthmm),cl1,cl]
                else:
                    pass
                #print(bb1,bb2)
                cx=cx*heightmm
                cl=cl*heightmm
                D1= dist.euclidean((b1, c1a), (b0, ca))
                #D2= dist.euclidean((b1, cl), (b2, cx))
                #D3= dist.euclidean((b1, cl), (b3, cx))
                x1=str(b1/widthmm)
                y1=str(c1a/heightmm)
                x2=str(b0/widthmm)
                y2=str(ca/heightmm)
                if line3==0:
                    l=D1
                    xaa=x1
                    xba=x2
                    yaa=y1
                    yba=y2
                else:
                    if D1<l:
                        l=D1
                        xa=x1
                        xb=x2
                        ya=y1
                        yb=y2
                    else:
                        pass

            l1 = "{:.2f}".format(l)
            num2=str(line2+1)
            num3=str(line3+1)
            l1=str(float(l1))
            f = open(result+'/'+txtfile, "a")
            try:
                #f.write(xaa+' '+yaa+' '+xba+' '+yba+' '+l1+' '+'mm'+"\n")
                f.write('distances on msc+ '+num2+' '+l1+' '+'mm'+"\n")
            except:
                pass
                #f.write(xa+' '+ya+' '+xb+' '+yb+' '+l1+' '+'mm'+"\n")
                #f.write('distances on msc+'+' '+l1+' '+'mm'+"\n")
                
    ##########
    def msp_msplus(fin,heightmm,widthmm,infile,result,txtfile):
        msplus=[]
        ms=[]
        for line1 in fin:
            first_char = line1[0]
            if first_char == "4":
                msplus.append(np.array(line1.split(), dtype=float)) 
            elif first_char == "0":
                ms.append(np.array(line1.split(), dtype=float)) 
            else:
                pass    
        for line2 in range (0,len(ms)):
            a=ms[line2][0]
            b=(ms[line2][1])
            b0=b*widthmm
            c=ms[line2][2]
            for line3 in range(0,len(msplus)):
                a1=msplus[line3][0]
                b01=(msplus[line3][1])
                b1=(msplus[line3][1])*widthmm
                c1=msplus[line3][2]
                b02=(b01-(msplus[line3][3]/2))*widthmm
                b03=(b01+(msplus[line3][3]/2))*widthmm
                b2=(b-(ms[line2][3]/2))*widthmm
                b3=(b+(ms[line2][3]/2))*widthmm
                if c<c1:
                    #print('c>c1')
                    cx=(c+((ms[line2][4])/2))
                    cx1=(c-(ms[line2][4]/2))
                    cl=(c1-((msplus[line3][4])/2))
                    cl1=(c1+(msplus[line3][4]/2))
                    bb1=[(b2/widthmm),(b3/widthmm),cx1,cx]
                    bb2=[(b02/widthmm),(b03/widthmm),cl,cl1]
                    if (bb1[0]<bb1[1]) & (bb1[2]<bb1[3]) & (bb2[0]<bb2[1]) & (bb2[2]<bb2[3]):
                        bb1=bb1
                        bb2=bb2
                    else:
                        bb1=[(b2/widthmm),(b3/widthmm),cx,cx1]
                        bb2=[(b02/widthmm),(b03/widthmm),cl1,cl]
                elif c>c1:
                    #print('c>c1')
                    cx=(c-(ms[line2][4]/2))
                    cx1=(c+(ms[line2][4]/2))
                    cl=(c1+(msplus[line3][4]/2))
                    cl1=(c1-(msplus[line3][4]/2))
                    bb1=[(b2/widthmm),(b3/widthmm),cx1,cx]
                    bb2=[(b02/widthmm),(b03/widthmm),cl,cl1]
                    if (bb1[0]<bb1[1]) & (bb1[2]<bb1[3]) & (bb2[0]<bb2[1]) & (bb2[2]<bb2[3]):
                        bb1=bb1
                        bb2=bb2
                    else:
                        bb1=[(b2/widthmm),(b3/widthmm),cx,cx1]
                        bb2=[(b02/widthmm),(b03/widthmm),cl1,cl]
                else:
                    pass
                iou=(caio.get_iou(bb1,bb2))*100
                iou=(caio.get_iou(bb1,bb2))*100
                if iou!=0:
                    iou_format = "{:.2f}".format(iou)
                    num2=str(line2+1)
                    num3=str(line3+1)
                    iou=str(float(iou_format))
                    f = open(result+'/'+txtfile, "a")
                    f.write('overlap on periapical '+num2+' '+iou+'%'+"\n")
                    f.close()
                else:
                    pass

        for line2 in range (0,len(ms)):
            a=ms[line2][0]
            b=(ms[line2][1])
            b0=b*widthmm
            c=ms[line2][2]
            ca=c*heightmm
            for line3 in range(0,len(msplus)):
                a1=msplus[line3][0]
                b01=(msplus[line3][1])
                b1=(msplus[line3][1])*widthmm
                c1=msplus[line3][2]
                c1a=c1*heightmm
                b02=(b01-(msplus[line3][3]/2))*widthmm
                b03=(b01+(msplus[line3][3]/2))*widthmm
                b2=(b-(ms[line2][3]/2))*widthmm
                b3=(b+(ms[line2][3]/2))*widthmm
                if c<c1:
                    cx=(c+((ms[line2][4])/2))
                    cx1=(c-(ms[line2][4]/2))
                    cl=(c1+((msplus[line3][4])/2))
                    cl1=(c1-(msplus[line3][4]/2))
                elif c>c1:
                    cx=(c-(ms[line2][4]/2))
                    cx1=(c+(ms[line2][4]/2))
                    cl=(c1-(msplus[line3][4]/2))
                    cl1=(c1+(msplus[line3][4]/2))
                else:
                    pass
                #print(bb1,bb2)
                cx=cx*heightmm
                cl=cl*heightmm
                cl1=cl1*heightmm
                D1= dist.euclidean((b1, cl), (b0, cx))
                D2= dist.euclidean((b1, cl), (b2, cx))
                D3= dist.euclidean((b1, cl), (b3, cx))
                D=[D1,D2,D3]
                l=min(D)
                if line3==0:
                    if l==D1:
                        l1=l
                        xaa=str(b1/widthmm)
                        xba=str(c1a/heightmm)
                        yaa=str(b0/widthmm)
                        yba=str(ca/heightmm)
                    elif l==D2:
                        l1=l
                        xaa=str(b1/widthmm)
                        xba=str(cl1/heightmm)
                        yaa=str(b2/widthmm)
                        yba=str(cx/heightmm)
                    else:
                        l1=l
                        xaa=str(b1/widthmm)
                        xba=str(cl1/heightmm)
                        yaa=str(b3/widthmm)
                        yba=str(cx/heightmm)
                else:
                    if l<l1:
                        if l==D1:
                            l1=l
                            xaa=str(b1/widthmm)
                            xba=str(c1a/heightmm)
                            yaa=str(b0/widthmm)
                            yba=str(ca/heightmm)
                        elif l==D2:
                            l1=l
                            xaa=str(b1/widthmm)
                            xba=str(cl1/heightmm)
                            yaa=str(b2/widthmm)
                            yba=str(cx/heightmm)
                        else:
                            l1=l
                            xaa=str(b1/widthmm)
                            xba=str(cl1/heightmm)
                            yaa=str(b3/widthmm)
                            yba=str(cx/heightmm)
                    else:
                        pass
                    
            l2 = "{:.2f}".format(l1)
            num2=str(line2+1)
            num3=str(line3+1)
            l2=str(float(l2))
            #m2=str(float(m2))
            f = open(result+'/'+txtfile, "a")
            try:
                #f.write(xaa+' '+yaa+' '+xba+' '+yba+' '+l2+' '+'mm'+"\n")
                f.write('distances on periapical '+num2+' '+l2+' '+'mm'+"\n")
            except:
                passs
                #f.write(xa+' '+ya+' '+xb+' '+yb+' '+l2+' '+'mm'+"\n")
                #f.write('distances on msp+'+' '+l2+' '+'mm'+"\n")
                
########
    def msb_msplus(fin,heightmm,widthmm,infile,result,txtfile):
        msplus1=[]
        ms=[]
        for line1 in fin:
            first_char = line1[0]
            if first_char == "6":
                msplus1.append(np.array(line1.split(), dtype=float)) 
            elif first_char == "0":
                ms.append(np.array(line1.split(), dtype=float))
            else:
                pass
            
        for line2 in range (0,len(ms)):
            a=ms[line2][0]
            b=(ms[line2][1])
            b0=b*widthmm
            c=ms[line2][2]
            z=[]
            for line3 in range(0,len(msplus1)):
                a1=msplus1[line3][0]
                b01=(msplus1[line3][1])
                b1=(msplus1[line3][1])*widthmm
                c1=msplus1[line3][2]
                b02=(b01-(msplus1[line3][3]/2))*widthmm
                b03=(b01+(msplus1[line3][3]/2))*widthmm
                b2=(b-(ms[line2][3]/2))*widthmm
                b3=(b+(ms[line2][3]/2))*widthmm
                if c<c1:
                    #print('c>c1')
                    cx=(c+((ms[line2][4])/2))
                    cx1=(c-(ms[line2][4]/2))
                    cl=(c1-((msplus1[line3][4])/2))
                    cl1=(c1+(msplus1[line3][4]/2))
                    bb1=[(b2/widthmm),(b3/widthmm),cx1,cx]
                    bb2=[(b02/widthmm),(b03/widthmm),cl,cl1]
                    if (bb1[0]<bb1[1]) & (bb1[2]<bb1[3]) & (bb2[0]<bb2[1]) & (bb2[2]<bb2[3]):
                        bb1=bb1
                        bb2=bb2
                    else:
                        bb1=[(b2/widthmm),(b3/widthmm),cx,cx1]
                        bb2=[(b02/widthmm),(b03/widthmm),cl1,cl]
                elif c>c1:
                    #print('c>c1')
                    cx=(c-(ms[line2][4]/2))
                    cx1=(c+(ms[line2][4]/2))
                    cl=(c1+(msplus1[line3][4]/2))
                    cl1=(c1-(msplus1[line3][4]/2))
                    bb1=[(b2/widthmm),(b3/widthmm),cx1,cx]
                    bb2=[(b02/widthmm),(b03/widthmm),cl,cl1]
                    if (bb1[0]<bb1[1]) & (bb1[2]<bb1[3]) & (bb2[0]<bb2[1]) & (bb2[2]<bb2[3]):
                        bb1=bb1
                        bb2=bb2
                    else:
                        bb1=[(b2/widthmm),(b3/widthmm),cx,cx1]
                        bb2=[(b02/widthmm),(b03/widthmm),cl1,cl]
                else:
                    pass
                iou=(caio.get_iou(bb1,bb2))*100
                if iou!=0:
                    iou1=iou
                    z.append(iou1)
                else:
                    pass

            zo=max(z)
            iou_format = "{:.2f}".format(zo)
            num2=str(line2+1)
            num3=str(line3+1)
            iouf=str(float(iou_format))
            f = open(result+'/'+txtfile, "a")
            f.write('overlap on bone loss '+num2+' '+iouf+'%'+"\n")
            #f.write(num2+num3+' '+iouf+'%'+"\n")
            f.close()
                    
               
                

#######
    def msw_msplus(fin,heightmm,widthmm,infile,result,txtfile):
        msplus=[]
        ms=[]
        for line1 in fin:
            first_char = line1[0]
            if first_char == "4":
                msplus.append(np.array(line1.split(), dtype=float)) 
            elif first_char == "9":
                ms.append(np.array(line1.split(), dtype=float))
            else:
                pass
            
        value=[]
        order=[]
        v=[]
        o=[]
        for line2 in range (0,len(ms)): #RCT
            a=ms[line2][0]
            b=(ms[line2][1])
            b0=b*widthmm
            c=ms[line2][2]
            ca=c*heightmm
            value1=[]
            order1=[]
            for line3 in range(0,len(msplus)): #Apex
                a1=msplus[line3][0]
                b01=(msplus[line3][1])
                b1=(msplus[line3][1])*widthmm
                c1=msplus[line3][2]
                c1a=c1*heightmm
                b02=(b01-(msplus[line3][3]/2))*widthmm
                b03=(b01+(msplus[line3][3]/2))*widthmm
                b2=(b-(ms[line2][3]/2))*widthmm
                b3=(b+(ms[line2][3]/2))*widthmm
                if c<c1:
                    cx=(c+((ms[line2][4])/2))
                    cx1=(c-(ms[line2][4]/2))
                    cl=(c1-((msplus[line3][4])/2))
                    cl1=(c1+(msplus[line3][4]/2))
                elif c>c1:
                    cx=(c-(ms[line2][4]/2))
                    cx1=(c+(ms[line2][4]/2))
                    cl=(c1+(msplus[line3][4]/2))
                    cl1=(c1-(msplus[line3][4]/2))
                else:
                    pass
                #print(bb1,bb2)
                cx=cx*heightmm
                cl=cl*heightmm
                D1= dist.euclidean((b1, c1a), (b0, ca))
                D2= dist.euclidean((b1, cl), (b2, cx))
                D3= dist.euclidean((b1, cl), (b3, cx))
                D=[D1,D2,D3]
                l=min(D)
                if line3==0:
                    if l==D1:
                        l1=l
                        xaa=str(b1/widthmm)
                        xba=str(c1a/heightmm)
                        yaa=str(b0/widthmm)
                        yba=str(ca/heightmm)
                    elif l==D2:
                        l1=l
                        xaa=str(b1/widthmm)
                        xba=str(cl/heightmm)
                        yaa=str(b2/widthmm)
                        yba=str(cx/heightmm)
                    else:
                        l1=l
                        xaa=str(b1/widthmm)
                        xba=str(cl/heightmm)
                        yaa=str(b3/widthmm)
                        yba=str(cx/heightmm)
                else:
                    if l<l1:
                        if l==D1:
                            l1=l
                            xaa=str(b1/widthmm)
                            xba=str(c1a/heightmm)
                            yaa=str(b0/widthmm)
                            yba=str(ca/heightmm)
                        elif l==D2:
                            l1=l
                            xaa=str(b1/widthmm)
                            xba=str(cl/heightmm)
                            yaa=str(b2/widthmm)
                            yba=str(cx/heightmm)
                        else:
                            l1=l
                            xaa=str(b1/widthmm)
                            xba=str(cl/heightmm)
                            yaa=str(b3/widthmm)
                            yba=str(cx/heightmm)
                    else:
                        pass
                if len(ms)==1 or len(msplus)==1:
                    if l1<=5:
                        num2=line2+1
                        num3=line3+1
                        v.append(l1)
                        o.append(l1)
                        o.append(num2)
                        o.append(num3)
                    else:
                        pass
                else:
                    if l1<=5:
                        num2=line2+1
                        num3=line3+1
                        value1.append(l1)
                        order1.append(l1)
                        order1.append(num2)
                        order1.append(num3)
                    else:
                        pass
            if len(ms)==1 or len(msplus)==1:
                pass
            else:
                value.append(value1)
                order.append(order1)

        if len(ms)==1 or len(msplus)==1:
            zo=min(v)
            n=o.index(zo)
            n2=o[n+1]
            n3=o[n+2]
            num_2=str(o[n+1])
            num_3=str(o[n+2])
            l2 = "{:.2f}".format(zo)
            l2=str(float(l2))
            f = open(result+'/'+txtfile, "a")
            try:
                #f.write(xaa+' '+yaa+' '+xba+' '+yba+' '+l2+' '+'mm'+"\n")
                f.write('distance apex '+num_3+' to RCT '+num_2+' '+l2+' '+'mm'+"\n")

            except:
                pass
        else:
            for line4 in range(0,len(ms)):
                try:
                    zo=min(value[line4])
                    order2=order[line4]
                    n=order2.index(zo)
                    n2=order2[n+1]
                    n3=order2[n+2]
                    num_2=str(order2[n+1])
                    num_3=str(order2[n+2])
                    l2 = "{:.2f}".format(zo)
                    l2=str(float(l2))
                    f = open(result+'/'+txtfile, "a")
                    try:
                        #f.write(xaa+' '+yaa+' '+xba+' '+yba+' '+l2+' '+'mm'+"\n")
                        f.write('distance apex '+num_3+' to RCT '+num_2+' '+l2+' '+'mm'+"\n")

                    except:
                        pass
                except:
                    pass

        for line2 in range (0,len(ms)):
            a=ms[line2][0]
            b=(ms[line2][1])
            b0=b*widthmm
            c=ms[line2][2]
            for line3 in range(0,len(msplus)):
                a1=msplus[line3][0]
                b01=(msplus[line3][1])
                b1=(msplus[line3][1])*widthmm
                c1=msplus[line3][2]
                b02=(b01-(msplus[line3][3]/2))*widthmm
                b03=(b01+(msplus[line3][3]/2))*widthmm
                b2=(b-(ms[line2][3]/2))*widthmm
                b3=(b+(ms[line2][3]/2))*widthmm
                if c<c1:
                    #print('c>c1')
                    cx=(c+((ms[line2][4])/2))
                    cx1=(c-(ms[line2][4]/2))
                    cl=(c1-((msplus[line3][4])/2))
                    cl1=(c1+(msplus[line3][4]/2))
                    bb1=[(b2/widthmm),(b3/widthmm),cx1,cx]
                    bb2=[(b02/widthmm),(b03/widthmm),cl,cl1]
                    if (bb1[0]<bb1[1]) & (bb1[2]<bb1[3]) & (bb2[0]<bb2[1]) & (bb2[2]<bb2[3]):
                        bb1=bb1
                        bb2=bb2
                    else:
                        bb1=[(b2/widthmm),(b3/widthmm),cx,cx1]
                        bb2=[(b02/widthmm),(b03/widthmm),cl1,cl]
                elif c>c1:
                    #print('c>c1')
                    cx=(c-(ms[line2][4]/2))
                    cx1=(c+(ms[line2][4]/2))
                    cl=(c1+(msplus[line3][4]/2))
                    cl1=(c1-(msplus[line3][4]/2))
                    bb1=[(b2/widthmm),(b3/widthmm),cx1,cx]
                    bb2=[(b02/widthmm),(b03/widthmm),cl,cl1]
                    if (bb1[0]<bb1[1]) & (bb1[2]<bb1[3]) & (bb2[0]<bb2[1]) & (bb2[2]<bb2[3]):
                        bb1=bb1
                        bb2=bb2
                    else:
                        bb1=[(b2/widthmm),(b3/widthmm),cx,cx1]
                        bb2=[(b02/widthmm),(b03/widthmm),cl1,cl]
                else:
                    pass
                iou=(caio.get_iou(bb1,bb2))*100
                if iou!=0:
                    iou_format = "{:.2f}".format(iou)
                    num2=str(line2+1)
                    num3=str(line3+1)
                    iou=str(float(iou_format))
                    f = open(result+'/'+txtfile, "a")
                    f.write('overlap apex '+num3+' to RCT '+ num2+' '+iou+'%'+"\n")
                    f.close()
                else:
                    pass