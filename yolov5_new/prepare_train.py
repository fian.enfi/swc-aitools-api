import boto3
import zipfile
import pandas as pd
import os
from PIL import Image
import cv2
from skimage import io
from skimage.filters import gaussian
#%matplotlib inline
from matplotlib import pyplot as plt

class collect_data_train:
    def s3toec2(bucket_name, zip_name, folderdata):
        s3 = boto3.client('s3')
        s3 = boto3.resource(
            service_name='s3',
            region_name='us-west-2',
            aws_access_key_id='AKIATLYOCRM7CJ6SXBNK',
            aws_secret_access_key='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'
        )
        # Download data directly from s3 bucket aws to EC2
        s3.Bucket(bucket_name).download_file(Key=zip_name, Filename=zip_name) # file name at s3 and file name will be save in
        # Convert zip file
        with zipfile.ZipFile(zip_name) as zip_ref: 
            zip_ref.extractall(folderdata) #path you will save it
    
    def prepro_img(path, copy2path, method):
        if method==1: #1 for CLAHE method
            for filename in os.listdir(path):
                img = cv2.imread(os.path.join(path, filename))
                try:
                    lab_img=cv2.cvtColor(img,cv2.COLOR_BGR2LAB)
                except:
                    pass
                l,a,b=cv2.split(lab_img)
                clahe=cv2.createCLAHE(clipLimit=5.0, tileGridSize=(8,8))
                clahe_img=clahe.apply(l)
                #equ=cv2.equalizeHist(l)
                #hist_update_img=cv2.cvtColor(update_img, cv2.COLOR_LAB2BGR)
                update_img=cv2.merge((clahe_img,a,b))
                CLAHE_IMG=cv2.cvtColor(update_img, cv2.COLOR_LAB2BGR)
                #plt.imshow(CLAHE_IMG)
                try:
                    cv2.imwrite(copy2path+filename,CLAHE_IMG)
                except:
                    pass
                
                
                
                