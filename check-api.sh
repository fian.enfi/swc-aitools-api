API_URL="http://162.240.6.124:9055/swc-ai-tools/ping"
PYTHON_SCRIPT="/home/developerswcdent/public_html/swc-dent-ai-tools-api/ai-tools-api-prod.py"
API_URL2="http://162.240.6.124:9056/swc-ai-tools/ping"
PYTHON_SCRIPT2="/home/developerswcdent/public_html/swc-dent-ai-tools-api/ai-tools-api-teeth_detector.py"
API_URL3="http://162.240.6.124:9057/swc-ai-tools/ping"
PYTHON_SCRIPT3="/home/developerswcdent/public_html/swc-dent-ai-tools-api/ai-tools-api-opg-teeth_detector_detail.py"
API_URL4="http://162.240.6.124:9058/swc-ai-tools/ping"
PYTHON_SCRIPT4="/home/developerswcdent/public_html/swc-dent-ai-tools-api/ai-tools-api-pa-teeth_detector_detail.py"

# Function to check if the API is responding
check_api() {
  current_datetime=$(date '+%Y-%m-%d %H:%M:%S')
  if curl -s --head $API_URL | grep "200 OK" > /dev/null; then
    echo "$current_datetime - 9055 API is up and running."
  else
    echo "$current_datetime - 9055 API is not responding. Starting $PYTHON_SCRIPT"
    nohup python3.9 $PYTHON_SCRIPT &
  fi
}

check_api2() {
  current_datetime=$(date '+%Y-%m-%d %H:%M:%S')
  if curl -s --head $API_URL2 | grep "200 OK" > /dev/null; then
    echo "$current_datetime - 9056 API is up and running."
  else
    echo "$current_datetime - 9056 API is not responding. Starting $PYTHON_SCRIPT2"
    nohup python3.9 $PYTHON_SCRIPT2 &
  fi
}

check_api3() {
  current_datetime=$(date '+%Y-%m-%d %H:%M:%S')
  if curl -s --head $API_URL3 | grep "200 OK" > /dev/null; then
    echo "$current_datetime - 9057 API is up and running."
  else
    echo "$current_datetime - 9057 API is not responding. Starting $PYTHON_SCRIPT3"
    nohup python3.9 $PYTHON_SCRIPT3 &
  fi
}

check_api4() {
  current_datetime=$(date '+%Y-%m-%d %H:%M:%S')
  if curl -s --head $API_URL4 | grep "200 OK" > /dev/null; then
    echo "$current_datetime - 9058 API is up and running."
  else
    echo "$current_datetime - 9058 API is not responding. Starting $PYTHON_SCRIPT4"
    nohup python3.9 $PYTHON_SCRIPT4 &
  fi
}


# Main loop to check the API periodically
while true; do
  check_api
  check_api2
  check_api3
  check_api4
  sleep 60  # Adjust the sleep time as needed (in seconds)
done

