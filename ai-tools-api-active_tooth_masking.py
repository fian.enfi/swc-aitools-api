from fastapi import FastAPI, Form, UploadFile, File
from fastapi.responses import FileResponse
import datetime
import calendar
import time
from random import randint
import cv2
from ultralytics import YOLO
import math
from PIL import ImageColor

import sys, os, distutils.core

app = FastAPI()


print("setup")
dist = distutils.core.run_setup("detectron2/setup.py")
sys.path.insert(0, os.path.abspath('detectron2'))

print("import detectron2")
import torch, detectron2
import cv2
import torch
import detectron2 
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data.catalog import Metadata
from detectron2.utils.visualizer import ColorMode
    
#load model
cfg = get_cfg()
cfg.MODEL.DEVICE = "cpu"
cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
cfg.MODEL.WEIGHTS = "models/model_final.pth"
cfg.MODEL.ROI_HEADS.NUM_CLASSES = 1
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5
predictor = DefaultPredictor(cfg)

msplus_metadata = Metadata()
msplus_metadata.set(thing_classes = ['Tooth'])


opg_tooth_numbering_class = [
    "11",
    "12",
    "13",
    "14",
    "15",
    "16",
    "17",
    "18",
    "21",
    "22",
    "23",
    "24",
    "25",
    "26",
    "27",
    "28",
    "31",
    "32",
    "33",
    "34",
    "35",
    "36",
    "37",
    "38",
    "41",
    "42",
    "43",
    "44",
    "45",
    "46",
    "47",
    "48",
    "51",
    "52",
    "53",
    "54",
    "55",
    "61",
    "62",
    "63",
    "64",
    "65",
    "71",
    "72",
    "73",
    "74",
    "75",
    "81",
    "82",
    "83",
    "84",
    "85",
    "SN",
]
global yolo_opg_segment_numbering_model
yolo_opg_segment_numbering_model = YOLO(
    "models/yolo_opg_segment_numbering.pt"
)

def createLog(idLog, log):
    # datetime object containing current date and time
    now = datetime.datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    print("LOG@"+dt_string+" ["+idLog +"] "+ log)

@app.post("/swc-ai-tools/1.0.0/active_tooth_masking")
def active_tooth_masking(image: UploadFile = File(...),active_tooth_numbers: str = Form(...)):
    active_tooth_numbers = active_tooth_numbers.split(",")
    tooth_colors = [
                "#7a98ab", "#f5c792", "#63a1a0", "#d47363", "#8dcf7e", "#b56aae", "#f8b85e", "#4977a8",
                "#a4c77d", "#e88b9c", "#5e8fbb", "#c3964f", "#7680a2", "#de8f7a", "#63b69d", "#c67eaf",
                "#8cb1d9", "#f2aa7c", "#6f8a64", "#c49679", "#58709f", "#d99758", "#9fae85", "#ea6880",
                "#508a74", "#af6e92", "#74b38a", "#e15c4b", "#77a8b5", "#d18e5c", "#6a967c", "#bd6f80"]

    firstImage = image
    mimetype = firstImage.content_type

    transactionId = "teeth_detection_" + str(calendar.timegm(time.gmtime())) + "_" +str(randint(1000000, 9999999))
    image_path = "ai-tools-api-tmp-image/" + transactionId
    fileNameFirstImage = "ai-tools-api-tmp-image/" + transactionId + "/" + firstImage.filename

    # Check whether the specified path exists or not
    if not os.path.exists(image_path):
        # Create a new directory because it does not exist 
        os.makedirs(image_path)
        #print("The new directory is created!")

    with open(fileNameFirstImage, "wb") as buffer:
        buffer.write(firstImage.file.read())

    result = ""
    if mimetype != "image/jpeg" and mimetype != "image/jpg" and mimetype != "image/png" and mimetype != "image/tiff":
        #print(mimetype)
        message = "The file you sent is not an image, please check the file again!"
        return message
    else:            
        # Input Image From Enhanced Image
        img_path = fileNameFirstImage

        img = cv2.imread(img_path)
        
        # Use the model to predict the mask
        outputs = predictor(img)

        # yolo tooth detection
        classes = opg_tooth_numbering_class
        results = yolo_opg_segment_numbering_model(img_path)
        for result in results:
            boxes = result.boxes  # Boxes object for bbox outputs
            masks = result.masks  # Masks object for segmentation masks outputs

        idx = []
        color = []
        for active_tooth_number in active_tooth_numbers:
            for index in range(len(boxes)):
                box_center = (int(boxes.data[index][0])+int(boxes.data[index][2])/2, int(boxes.data[index][1])+int(boxes.data[index][3])/2)    
                tooth_number = str(classes[int(boxes.cls[index].cpu().item())])
            
                #print("tooth data:", tooth_number, active_tooth_number)
                if tooth_number == active_tooth_number:
                    #tooth number in active tooth numbers, detect the index of the tooth
                    i = 0
                    j = 0
                    dist = 99999999
                    for box in outputs['instances'].pred_boxes.tensor.numpy():
                        #print("box", i, box)
                        b_center = (int(box[0])+int(box[2])/2, int(box[1])+int(box[3])/2)
                        new_dist = math.dist(b_center, box_center)
                        #print("new_dist", new_dist, dist)
                        if new_dist < dist:
                            dist = new_dist
                            j = i
                        i = i + 1
                    idx.append(j)
                    #print("idx", idx)
            
        contours = []
        #print("idx", idx)
        
        for i in idx:
            #collect the active tooth mask
            pred_mask = outputs['instances'].pred_masks[i]
            # pred_mask is of type torch.Tensor, and the values are boolean (True, False)
            # Convert it to a 8-bit numpy array, which can then be used to find contours
            mask = pred_mask.numpy().astype('uint8')
            contour, _ = cv2.findContours(mask, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

            contours.append(contour[0])

        image_with_overlaid_predictions = img.copy()

        for i in range(len(contours)):
            color = ImageColor.getrgb(tooth_colors[i])
            cv2.drawContours(image_with_overlaid_predictions, [contours[i]], -1, (color[2],color[1],color[0]), 3)

        # save the image to a jpg file
        cv2.imwrite(image_path + '/output.jpg', image_with_overlaid_predictions)

        file = FileResponse(image_path + "/output.jpg", media_type='image/jpeg')
        
        return file

@app.get("/ping")
def ping():
    response_data = {
        'message': "pong"           
    }

    return response_data